import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://react-homework64.firebaseio.com/'
});

export default instance;