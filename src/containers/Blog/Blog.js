import React, {Component} from 'react';
import './Blog.css';
import axios from "../../axios-orders";
import Loader from "../../components/UI/Loader/Loader";
import PostBox from "../../components/PostBox/PostBox";
import {Route} from "react-router-dom";

class Blog extends Component {
  state = {
    posts: [],
    loading: true
  };

  getPosts = () => {
    this.setState({loading: true});

    axios.get('posts.json').then(response => {
      let posts = [];

      for (let key in response.data) {
        let post = response.data[key];
        post.id = key;
        posts.push(post);
      }

      this.setState({posts, loading: false});
    });
  };

  componentDidMount() {
    this.getPosts();
  }

  // componentDidUpdate() {
  //   this.getPosts();
  // }

  render() {
    return (
      <div className="Blog">
        {this.state.loading ? <Loader /> : this.state.posts.map(post => (
          <Route path="/posts" render={props => <PostBox key={post.id} post={post} {...props}/>} />
        )).reverse()}
      </div>
    );
  }
}

export default Blog;