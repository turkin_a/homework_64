import React, { Component } from 'react';
import './App.css';
import {Switch, Route} from "react-router-dom";

import MainNav from "./components/MainNav/MainNav";
import Blog from "./containers/Blog/Blog";
import PostEdit from "./components/PostEdit/PostEdit";
import Post from "./components/Post/Post";

class App extends Component {
  render() {
    return (
      <div className="App">
        <MainNav />
        <Switch>
          <Route path="/" exact component={Blog} />
          <Route path="/posts" exact component={Blog} />
          <Route path="/posts/add" exact component={PostEdit} />
          <Route path="/posts/:id" exact component={Post} />
          {/*<Route path="/posts/add" render={props => <PostEdit {...props} />} />*/}
          {/*<Route path="/about" exact component={About} />*/}
          {/*<Route path="/contacts" exact component={Contacts} />*/}
          <Route render={() => <h1>Page not found</h1>} />
        </Switch>
      </div>
    );
  }
}

export default App;