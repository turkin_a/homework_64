import React, {Component} from 'react';
import './PostEdit.css';
import axios from "../../axios-orders";
import Button from "../UI/Button/Button";

class PostEdit extends Component {
  state = {
    currentPost: {
      title: '',
      description: '',
      datetime: ''
    }
  };

  onChangeHandler = (event) => {
    let currentPost = {...this.state.currentPost};
    currentPost[event.target.name] = event.target.value;
    currentPost.datetime = new Date();

    this.setState({currentPost});
  };

  addPost = () => {
    axios.post('posts.json', this.state.currentPost)
      .finally(() => this.props.history.replace('/posts'));
  };

  abortChanges = () => {
    this.props.history.replace('/posts');
  };

  componentDidMount() {

  }

  render() {
    return (
      <div className="PostEdit">
        <h3 className="PageTitle">Add new post</h3>
        <p>Title:</p>
        <input
          type="text"
          name="title"
          className="InputTitle"
          value={this.state.title}
          onChange={(event) => this.onChangeHandler(event)}
        />
        <p>Description</p>
        <textarea
          name="description"
          className="InputDescription"
          value={this.state.description}
          onChange={(event) => this.onChangeHandler(event)}
        />
        <Button className={'FormBtn'} clicked={this.addPost}>Save</Button>
        <Button className={'FormBtn'} clicked={this.abortChanges}>Cancel</Button>
      </div>
    );
  }
}

export default PostEdit;