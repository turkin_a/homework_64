import React, {Component} from 'react';
import './Post.css';
import axios from "../../axios-orders";

class Post extends Component {
  state = {
    post: {}
  };

  componentDidMount() {
    if (this.props.match.params.id) {
      axios.get(`posts/${this.props.match.params.id}.json`).then(response => {
        this.setState({post: response.data});
      });
    }
  };

  render() {
    return (
      <div className="Post">
        <p>{this.state.post.datetime}</p>
        <h3>{this.state.post.title}</h3>
        <p>{this.state.post.description}</p>
      </div>
    );
  }
}

export default Post;