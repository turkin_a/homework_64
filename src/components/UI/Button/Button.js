import React, { Component } from 'react';
import './Button.css';

class Button extends Component {
  shouldComponentUpdate(newProps) {
    return (newProps.children !== this.props.children);
  }

  render() {
    return <button type="button" className="Button" onClick={() => this.props.clicked()}>{this.props.children}</button>;
  }
}

export default Button;