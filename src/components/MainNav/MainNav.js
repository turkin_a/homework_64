import React, {Component} from 'react';
import './MainNav.css';
import {NavLink} from "react-router-dom";

class MainNav extends Component {
  render() {
    return (
      <div className="MainNav">
        <h2 className="MainTitle">My Blog</h2>
        <ul className="NavList">
          <li><NavLink to="/posts" exact className="NavItem" activeClassName="Selected">Home</NavLink></li>
          <li><NavLink to="/posts/add" exact className="NavItem" activeClassName="Selected">Add</NavLink></li>
          <li><NavLink to="/about" exact className="NavItem" activeClassName="Selected">About</NavLink></li>
          <li><NavLink to="/contacts" exact className="NavItem" activeClassName="Selected">Contacts</NavLink></li>
        </ul>
      </div>
    );
  }
}

export default MainNav;