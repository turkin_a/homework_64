import React, {Component} from 'react';
import './PostBox.css';
import Button from "../UI/Button/Button";
import {NavLink} from "react-router-dom";

class PostBox extends Component {
  clicked = () => {

  };

  render() {
    return (
      <div className="PostBox">
        <p className="PostTime">Created on: {this.props.post.datetime}</p>
        <h4 className="PostTitle">{this.props.post.title}</h4>
        <Button clicked={this.clicked}>
          <NavLink to={`/posts/${this.props.post.id}`} className="ReadMoreBtn" exact >Read more >></NavLink>
        </Button>
      </div>
    );
  }
}

export default PostBox;